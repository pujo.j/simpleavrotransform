package com.sfeir.data.simpleavrotransform;

import org.apache.avro.Schema;

public class SchemaUtils {

    public static Schema toSchema(String input) {
        return new Schema.Parser().parse(input);
    }

    public static String fromSchema(Schema s) {
        return s.toString(true);
    }
}
