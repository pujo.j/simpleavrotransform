package com.sfeir.data.simpleavrotransform;

import com.google.api.gax.paging.Page;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Storage;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AvroInput {

    private static Logger log = LoggerFactory.getLogger(AvroInput.class);
    private final Storage storage;
    private Schema schema;
    private List<BlobId> blobs = new ArrayList<>();

    public AvroInput(Storage storage, String bucket, String prefix) throws IOException {
        this.storage = storage;
        Page<Blob> blobPage = storage.list(bucket, Storage.BlobListOption.currentDirectory(), Storage.BlobListOption.prefix(prefix));
        blobPage.getValues().forEach(blob -> blobs.add(blob.getBlobId()));
        while (blobPage.hasNextPage()) {
            blobPage = blobPage.getNextPage();
            blobPage.getValues().forEach(blob -> blobs.add(blob.getBlobId()));
        }
        // Read schema from first file
        if (blobs.size() > 0) {
            BlobId blob = blobs.get(0);
            Long size = storage.get(blob).getSize();
            SeekableBlob blobReader = new SeekableBlob(storage.reader(blobs.get(0)), size);
            DatumReader<GenericRecord> reader = new GenericDatumReader<>();
            DataFileReader<GenericRecord> fileReader = new DataFileReader<>(blobReader, reader);
            schema = fileReader.getSchema();
            fileReader.close();
            blobReader.close();
        } else {
            // Empty schema...
            this.schema = SchemaBuilder.record("empty").fields().endRecord();
        }
    }

    public String getSchema() {
        return SchemaUtils.fromSchema(schema);
    }

    public Iterator<GenericRecord> records() throws IOException {
        return new AvroIterator(this.storage, this.blobs);
    }

}
