package com.sfeir.data.simpleavrotransform;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;

import java.util.concurrent.Callable;

@CommandLine.Command(description = "Transforms GCS stored Avro files",
        name = "avro-transform"
)
public class Main implements Callable<Void> {

    @CommandLine.Option(names = {"--input", "-i"}, required = true)
    private String inputBucket;
    @CommandLine.Option(names = {"--output", "-o"}, required = true)
    private String outputBucket;
    @CommandLine.Option(names = {"--script", "-s"}, required = true)
    private String script;
    @CommandLine.Option(names = {"-v", "--verbose"}, description = "Debug logging")
    public boolean verbose = false;

    public static void main(String[] args) {
        new CommandLine(new Main()).execute(args);
    }

    @Override
    public Void call() throws Exception {
        if (verbose) {
            LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
            StatusPrinter.printInCaseOfErrorsOrWarnings(context);
            context.getLogger("ROOT").setLevel(Level.DEBUG);
        }
        Binding binding = new Binding();
        Logger logger = LoggerFactory.getLogger("Script");
        Storage storage = StorageOptions.getDefaultInstance().getService();
        Context context = new Context(storage, inputBucket, outputBucket);
        binding.setVariable("context", context);
        binding.setVariable("log", logger);
        GroovyShell shell = new GroovyShell(binding);
        Script script = shell.parse(this.script);
        try {
            script.run();
        } catch (Throwable t) {
            logger.error(t.getMessage(), t);
            throw t;
        }
        return null;
    }
}
