package com.sfeir.data.simpleavrotransform;

import com.google.cloud.storage.Storage;

import java.io.IOException;

public class Context {
    private Storage storage;
    private String outputBucket;
    private String inputBucket;

    public Context(Storage storage, String outputBucket, String inputBucket) {
        this.storage = storage;
        this.outputBucket = outputBucket;
        this.inputBucket = inputBucket;
    }

    public AvroOutput writeAvros(String prefix, String schema) throws IOException {
        return new AvroOutput(storage, outputBucket, prefix, schema);
    }

    public AvroInput readAvros(String prefix) throws IOException {
        return new AvroInput(storage, inputBucket, prefix);
    }

}


