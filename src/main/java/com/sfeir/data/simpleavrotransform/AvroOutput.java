package com.sfeir.data.simpleavrotransform;

import com.google.cloud.WriteChannel;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.common.io.CountingOutputStream;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.generic.GenericRecordBuilder;
import org.apache.avro.io.DatumWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.channels.Channels;
import java.text.NumberFormat;

public class AvroOutput {

    private static Logger log = LoggerFactory.getLogger(AvroOutput.class);

    private static long BlockSize = 256 * 1024 * 1024; // 256 Mo
    private final Storage storage;

    private String bucket;
    private String prefix;

    private int currentFileNo = 1;
    private WriteChannel currentWriteChannel;
    private CountingOutputStream currentOutputStream;
    private DataFileWriter<GenericRecord> currentFileWriter;
    private Schema schema;
    private NumberFormat formatter;

    public AvroOutput(Storage storage, String bucket, String prefix, String schema) throws IOException {
        this.storage = storage;
        this.bucket = bucket;
        this.prefix = prefix;
        this.formatter = NumberFormat.getIntegerInstance();
        this.formatter.setGroupingUsed(false);
        formatter.setMinimumIntegerDigits(4);
        formatter.setMaximumFractionDigits(4);
        this.schema = SchemaUtils.toSchema(schema);
        openNewFile();
    }

    private Blob blob;

    public void write(GenericRecordBuilder recordBuilder) throws IOException {
        currentFileWriter.append(recordBuilder.build());
        if (this.currentOutputStream.getCount() > BlockSize) {
            closeCurrentFile();
            currentFileNo++;
            openNewFile();
        }
    }

    public void flush() throws IOException {
        if (currentFileWriter != null) {
            currentFileWriter.flush();
            currentOutputStream.flush();
        }
    }

    public void close() throws IOException {
        this.flush();
        closeCurrentFile();
    }

    private void openNewFile() throws IOException {
        BlobId blobId = BlobId.of(bucket, this.prefix + "_" + formatter.format(currentFileNo) + ".avro");
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("avro/binary").build();
        currentWriteChannel = storage.writer(blobInfo);
        this.currentOutputStream = new CountingOutputStream(Channels.newOutputStream(currentWriteChannel));
        DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<>(schema);
        currentFileWriter = new DataFileWriter<>(datumWriter);
        currentFileWriter.create(schema, currentOutputStream);
    }

    private void closeCurrentFile() throws IOException {
        log.debug("Wrote file {}", this.prefix + "_" + formatter.format(currentFileNo));
        currentFileWriter.close();
        currentOutputStream.close();
        currentWriteChannel.close();
    }

}
