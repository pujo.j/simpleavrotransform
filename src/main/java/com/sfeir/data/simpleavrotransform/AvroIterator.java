package com.sfeir.data.simpleavrotransform;

import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Storage;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class AvroIterator implements Iterator<GenericRecord> {

    private final Storage storage;
    private List<BlobId> blobs;
    private SeekableBlob currentBlobReader;
    private int currentBlob = -1;
    private boolean finished = false;
    private DataFileReader<GenericRecord> currentReader;

    public AvroIterator(Storage storage, List<BlobId> blobs) throws IOException {
        this.storage = storage;
        this.blobs = blobs;
        if (blobs.size() > 0) {

            OpenNextBlob();
        } else {
            finished = true;
        }
    }

    private synchronized void OpenNextBlob() throws IOException {
        if (currentReader != null) {
            currentReader.close();
        }
        if (currentBlobReader != null) {
            currentBlobReader.close();
        }
        currentBlob++;
        if (currentBlob >= blobs.size()) {
            finished = true;
        } else {
            BlobId blob = blobs.get(currentBlob);
            Long size = storage.get(blob).getSize();
            currentBlobReader = new SeekableBlob(storage.reader(blobs.get(currentBlob)), size);
            DatumReader<GenericRecord> reader = new GenericDatumReader<>();
            currentReader = new DataFileReader<>(currentBlobReader, reader);
        }
    }

    @Override
    public boolean hasNext() {
        if (finished) {
            return false;
        }
        if (currentReader.hasNext()) {
            return true;
        } else {
            try {
                OpenNextBlob();
            } catch (IOException e) {
                finished = true;
                return false;
            }
            return !finished;
        }
    }

    @Override
    public GenericRecord next() {
        if (finished) {
            throw new NoSuchElementException();
        }
        if (currentReader.hasNext()) {
            return currentReader.next();
        } else {
            try {
                OpenNextBlob();
            } catch (IOException e) {
                finished = true;
                throw new NoSuchElementException();
            }
            return currentReader.next();
        }
    }
}
