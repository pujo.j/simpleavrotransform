package com.sfeir.data.simpleavrotransform;

import com.google.cloud.ReadChannel;
import org.apache.avro.file.SeekableInput;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicLong;

public class SeekableBlob implements SeekableInput {
    private ReadChannel channel;
    private final Long size;
    private AtomicLong currentPosition = new AtomicLong(0);
    private ByteBuffer bb = null;
    private byte[] bs = null;
    private byte[] b1 = null;

    public SeekableBlob(ReadChannel channel, Long size) {
        this.channel = channel;
        this.size = size;
    }

    @Override
    public void seek(long p) throws IOException {
        if (p > size) {
            throw new IOException("Seek out of bounds");
        }
        channel.seek(p);
        currentPosition.set(p);
    }

    @Override
    public long tell() throws IOException {
        return currentPosition.get();
    }

    @Override
    public long length() throws IOException {
        return size;
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        if (off >= 0 && off <= b.length && len >= 0 && off + len <= b.length && off + len >= 0) {
            if (len == 0) {
                return 0;
            } else {
                ByteBuffer var4 = this.bs == b ? this.bb : ByteBuffer.wrap(b);
                var4.limit(Math.min(off + len, var4.capacity()));
                var4.position(off);
                this.bb = var4;
                this.bs = b;
                return channel.read(var4);
            }
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public void close() throws IOException {
        channel.close();
    }
}
